# README #

readme

Solutions\nodejs\happylittleanimals

* To run:
* npm install
* node game.js

Server runs on:
var ip_addr = 'localhost';
var port    =  '49300';

### What is this repository for? ###

* Twenty questions implementation
* Stripped data from wordnik and use animal definitions plus glossary to populate a mongodb
* other data implementations use wordnik "related words" and text razor (not working)
* use a terrible algorithm to start asking questions!


## Twenty Questions: ##

### Goal: ###

* Use node.js and mongodb as tools to implement design.

* Spend as little time entering/manipulating data as possible

* Don’t give up

Not knowing node or mongodb, most of my time was spent with syntax, learning mongodb and mongoose, etc.

### Data: ###

I spent most of my time finding a data source. I did not want, in any way, to be entering animal data into a database, but I also wanted the average user to answer the questions (i.e. do not use a scientific classification).

** Attempt 1:**

Wordnik - related words:

In app.js, you can see the first attempt where I read in each animal (after doing a quick cleanup of the animal list) and then do a lookup on wordnik to find all of the “related words”

http://developer.wordnik.com/docs.html#!/word/getRelatedWords_get_4

For each animal, store the related words and the top words for animal groups would bubble to the top, right? Not in my finding.  Most of the related words were just other animals and coming up with generic questions like “Does your animal make you think lizard?” just wouldn’t work too well in finding common attributes.  I need descriptive words.

** Attempt 2:**

Wordnik definition (American Heritage dictionary) + Text Razor (parts of speech):

In textrazor.js you can see the next iterations.  The textrazor attempt was to take a wordnik definition and send it to textrazor to find the nouns/adjectives associated with each animal’s definition.

http://developer.wordnik.com/docs.html#!/word/getDefinitions_get_2

api.textrazor.com

The text razor api would only allow two concurrent connections (unless I wanted to spend a million dollars) so after trying to throttle that down, I gave up and went with http://text-processing.com.  This I also couldn’t get to work the way I wanted, so attempt #3.

**Attempt 3:**

Wordnik definition + glossary:

My final data gathering was done with the wordnik definition + a node plugin called glossary

https://github.com/harthur/glossary

This stripped out “relevant” words from the definition and didn’t require another api call.  Glossary isn’t perfect but it game me a good data set to start with.  While playing the game with this data, I had  to clean up some things like convert plurals to singular etc so that mammals and mammal had the same weight etc.

However this still wasn’t great, and admittedly, I had to spend a few hours going through my db with some final attribute additions especially for things like “Fly” where the definition returned back “to soar” etc.!!

### Algorithm (if you can call it that): ###

game.js is a crude implementation of guessing the animals based on the data above.  I use just two question forms so that the game didn’t need to think about what attribute it was asking about.

** Try to answer:**

Before asking a question (except at the start of the game) the game checks if it should try to answer.

*Narrow down - generateAnswer:*

In the narrow down approach, I tried to aggregate my attributes and as I attributes were confirmed, I eliminated animals.  This REALLY fails with my random data collection because while most mammals always had mammal in their definition, not all of them did so I would immediately eliminate the correct animal.

This can be seen in generateAnswer where I took a brute force approach where once there are less than 4 animals left, start asking one by one and if is nothing is left, wipe out all the attributes and try again.  Lame.

*Collect data - generateAnswer2*

With such a random set of attributes, collecting data was the best way to go, but not very efficient.  As questions are asked, if “Yes” is returned, give that animal an uptick in confirmed attributes.  This allows me to not care about No and skip (although that data could be collected to help a “learning” algorithm).  When animals match more than 3 attributes, start guessing animals that have not been guessed before.

Finally, don’t give up.  If there are only a couple questions left, just try to answer with what we know so far. With the random data and random question, there will most likely be a few animals that are the top matches, so the game should give itself a few tries.

** Ask a question: **

Asking a question is where the db came into play. I would ask mongo to unwind the attributes for each animal in an aggregate and give me the most popular attributes only where an attribute was repeated more than 3 times. Granted, I do not have to ask the db each time as the code currently does (the list is static).

I added a bit of randomness to asking a question where every 3rd question is any attribute matching the above and all other questions are done in order of popularity. The randomness is a hope to get lucky by requesting an attribute that happens to be a 'yes' for the animal being played but for no more than a few other animals. This also gives the illusion that the game isn't just constantly try the same questions. However, it also prevents the game from winning as we might waste a few questions. I prevent the game from asking about the same attribute by storing the already confirmed attributes for that game.

### Changes for improvement: ###

1. The issue with my approach is it does not eliminate redundant questions. i.e. If you are a bird, I don’t need to ask if you are a mammal.  Or if you are in the ocean, I don’t need to ask if you like water.  When an attribute comes back with a “yes” response, add the redundant attributes to the confirmedAttribute list as well so that they are not asked in subsequent questions.

1. Right now the previous games are wiped out at startup. This could be tweaked to learn from previous games using the confirmed attributes, guessed animals etc and over time a better algorithm could be based on wins and losses.

1. The code is ugly and can easily be refactored in a few places, but its impressive that even with the duplication, the game is ~400 lines long.

1. Testing. With the randomness factor, I guess winning wasn't always the end goal, but it would be curious to think about ways to test what is an acceptable win rate.