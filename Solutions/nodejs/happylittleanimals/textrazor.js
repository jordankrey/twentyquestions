var application_root = __dirname,
    path = require("path"),
    mongoose = require('mongoose');

var express = require('express'), 
	http = require('http');


var app = express(); 
var server = http.createServer(app);

// Database
mongoose.connect('mongodb://localhost:27017/animals');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
  // yay!
});

var animalSchema = mongoose.Schema({
    name: String,
    attributes: [String],
    definition: String,
    textRazorWords: String
});

var Animal = mongoose.model('Animal', animalSchema);

console.log('Removing data...');
Animal.remove({}, function(err) {
	if (err) {
		console.log(err);
	}
});


var fs = require('fs');
var input = fs.createReadStream('<path to animals txt>');
readLines(input, storeAttributes);
//storeAttributes('gorilla');

function readLines(input, func) {
  var remaining = '';

  input.on('data', function(data) {
    remaining += data;
    var index = remaining.indexOf('\n');
    var last  = 0;
    while (index > -1) {
      var line = remaining.substring(last, index);
      last = index + 1;
      func(line);
      index = remaining.indexOf('\n', last);
    }

    remaining = remaining.substring(last);
  });

  input.on('end', function() {
    if (remaining.length > 0) {
      func(remaining);
    }
  });
}

function storeAttributes(data) {
	var name = data.replace(/\r$/,'').toLowerCase();

    url = "http://api.wordnik.com:80/v4/word.json/" + name + "/definitions?limit=200&partOfSpeech=noun&includeRelated=true&sourceDictionaries=ahd&useCanonical=false&includeTags=false&api_key=<api key>";

    console.log(url);

	// get is a simple wrapper for request()
	// which sets the http method to GET
	var request = http.get(url, function (response) {
	    // data is streamed in chunks from the server
	    // so we have to handle the "data" event    
	    var buffer = "", 
	        data,
	        route;

	    response.on("data", function (chunk) {
	        buffer += chunk;
	    }); 

	    response.on("end", function (err) {
	        // finished transferring data
	        // dump the raw data
	        console.log(name + ': ' + buffer + "\n Parsed: " + JSON.parse(buffer));
	        console.log("\n");
	        data = JSON.parse(buffer);
	        var definition;

	        if (typeof(data) !== 'undefined' && typeof(data[0]) !== 'undefined') {
	        	definition = data[0].text;	
	        }

	        textToGlossary(definition, function(attributes) {
		        var animal = new Animal({ name: name, definition: definition, attributes: attributes  });
				animal.save(function (err, animal) {
				  if (err) return console.error(err);
				  console.log('Saved: ' + animal);
				});
			});
			
	    }); 
	}); 
};


function textRazor(definitionInput, callback) {

    if (typeof(definitionInput) === 'undefined') {
    	callback(null);
    	return;
    }

	var definition = definitionInput.toLowerCase();

	var querystring = require('querystring');
	var fs = require('fs');
	var options = {
	  host: 'api.textrazor.com',
	  port: '80',
	  path: '/',
	  method: 'POST',
	  headers: {
	  	  'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Length': Buffer.byteLength(definition, 'utf8'),
      }
	};

	
	var req = http.request(options, function(response) {
	  var str = ''
	  response.on('data', function (chunk) {
	    str += chunk;
	  });

	  response.on('end', function () {
	    callback(str);
	  });
	});

	 var post_data = querystring.stringify({
      'apiKey' : 'DEMO',
      'extractors': 'words',
      'text': definition
  	});

	console.log(post_data);

	//This is the data we are posting, it needs to be a string or a buffer
	req.write(post_data);
	req.end();


};

function textProcessing(definitionInput, callback) {

	var unirest = require('unirest');
	var Request = unirest.post("http://text-processing.com/api/tag/")
	  .headers({ 
	  })
	  .send({ 
	    "language": "english",
	    "output": "iob",
	    "text": definitionInput
	  })
	  .end(function (response) {
	    console.log(response.body);

	  });

};

function textToGlossary(definitionInput, callback) {
    if (typeof(definitionInput) === 'undefined') {
    	callback(null);
    	return;
    }

	var glossary = require("glossary")({
	   blacklist: ["genus", "genera", "World", "legs", "body", "Old", "species", "order", 
	   "neck", "head", "two", "regions", "ruminant", "C", "class", "eyes", "pairs", "antelopes", 
	   "limbs", "food", "(Panthera", "one", "forelimbs", "four", "five", "ruminant mammals", "two pairs",
	   	"New", "males", "breeds", "feet", "Hemisphere", "spines", "size", "animals", "skin", "North", "South"]
	});

	var keywords = glossary.extract(definitionInput);

	console.log(keywords); 

	callback(keywords);

};

