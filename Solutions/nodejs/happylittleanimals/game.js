var restify = require('restify');
var mongoose = require('mongoose');
var async = require('async');

 
//var ip_addr = 'localhost';
var port    =  '49300';
 
var server = restify.createServer({
    name : "Happy Little Animals"
});
 
server.listen(port , function(){
    console.log('%s listening at %s ', server.name , server.url);
});

server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(restify.CORS());

mongoose.connect('mongodb://localhost:27017/animals');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
  console.log("Connected to db.")
});

// database schema
var animalSchema = mongoose.Schema({
    name: String,
    attributes: [String]
});

var gameSchema = mongoose.Schema({
	gameId: String,
    currentQuestion: Number,
    currentAttribute: String,
    confirmedAttributes: [String],
    animalsMatchingAttributes: Object,
    answer: String,
    bruteForce: Boolean,
    bruteAnimalsToGuess: [String],
    guessedAttributes: [String],
    guessedAnimals: [String]
});


//models
var Animal = mongoose.model('Animal', animalSchema);
var Game = mongoose.model('Game', gameSchema);

console.log('Removing games...');
Game.remove({}, function(err) {
	if (err) {
		console.log(err);
	}
})

//response models
function GameNameResponse(name) {
	this.Name = name;
}

function GameResponse(id, type, question) {
	this.Id = id;
	this.Type = type;
	this.Question = question;
}

var PATH = '/api'

server.get({path : PATH + '/name', version : '0.0.1'} , findServerName);
server.post({path : PATH + '/game/:gameId/start', version : '0.0.1'} , startNewGame);
server.post({path : PATH + '/game/:gameId/answer/:questionid/:response', version : '0.0.1'} , postAnswer);
server.get({path : PATH + '/animal/:name', version : '0.0.1'} , getAnimal);

function findServerName(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin','*');

	var gameName = new GameNameResponse(server.name);

	console.log(gameName);
    res.send(200 , gameName);
    return next();
}

function getAnimal(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin','*');

	Animal.findOne({name: req.params.nameId}, function (error, animal) {
	  if (error) {
	  	return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)));
  	  } else {
		   res.send(201 , animal);
		   return next();
	  }
	});
}

function startNewGame(req , res , next){
	console.log('Starting new game.. ' + req.params.gameId);
    var game = new Game();
    game.gameId = req.params.gameId;
    game.currentQuestion = 1;
    game.bruteForce = false;
    game.confirmedAttributes = [];
    game.guessedAttributes = [];
    game.animalsMatchingAttributes = {};
 
    res.setHeader('Access-Control-Allow-Origin','*');
 
    game.save(function(err , success){
        console.log('Response success '+success);
        console.log('Response error '+err);
        if(success){
        	console.log(game);

			var defaultResponse = new GameResponse(game.currentQuestion, "GiveUp", 'Well, I tried.');
		
			generateQuestion(game, defaultResponse, function(response) {
				game.markModified('animalsMatchingAttributes');
				game.save();
				res.send(201 , response);

				return next();		
			});

        }else{
        	console.log(err);
            return next(err);
        }
    });
}

function postAnswer(req , res , next){
	var gameId = req.params.gameId;
	var questionId = req.params.questionid;
	var response = req.params.response;

 	res.setHeader('Access-Control-Allow-Origin','*');
	console.log('Response: ' + response + ' for game: ' + gameId);

	if ('win' === response || 'lose' === response) {
		res.send(200 , 'Fantastic.');
		return next();
	}

	Game.findOne({gameId: gameId}, function (error, game) {
	  if (error) {
	  	return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))
	  }
	  if (game) {
	  	console.log(game);

		if ('yes' === response || 'no' === response) {
	    	game.currentQuestion = game.currentQuestion + 1;
		}

		if ('yes' === response) {
			game.confirmedAttributes.push(game.currentAttribute);
			Animal.find({attributes: { $in : [game.currentAttribute] } }, function (error, animals) {
			  if (error) {
			  	return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))
			  }
		  	  else {
				   animals.forEach( function(animal) {
				   		if ( !game.animalsMatchingAttributes.hasOwnProperty(animal.name) ) {
				   			var animalCount = {};
				   			animalCount[animal.name] = 1;
				   			game.animalsMatchingAttributes[animal.name] = animalCount;
				   		} else {
				   			game.animalsMatchingAttributes[animal.name][animal.name] = game.animalsMatchingAttributes[animal.name][animal.name] + 1;
				   		}
				   		console.log('Animal with attribute: ' + animal.name + ' now matches: ' + game.animalsMatchingAttributes[animal.name][animal.name]);
				   });

				   var defaultResponse = new GameResponse(game.currentQuestion, "GiveUp", 'Well, I tried.');

					generateAnswer2(game, defaultResponse, function(response) {
						if (response && typeof(response) !== 'undefined') {
							game.markModified('animalsMatchingAttributes');
							game.guessedAnimals.push(game.answer);
							game.save();
							res.send(201 , response);

							return next();
						}		
					});
				
					generateQuestion(game, defaultResponse, function(response) {
						game.markModified('animalsMatchingAttributes');
						game.save();
						res.send(201 , response);

						return next();		
					});
			  }
			});		
		} else {
				var defaultResponse = new GameResponse(game.currentQuestion, "GiveUp", 'Well, I tried.');

				generateAnswer2(game, defaultResponse, function(response) {
					if (response && typeof(response) !== 'undefined') {
						game.markModified('animalsMatchingAttributes');
						game.guessedAnimals.push(game.answer);
						game.save();
						res.send(201 , response);

						return next();
					}		
				});

				generateQuestion(game, defaultResponse, function(response) {
						game.markModified('animalsMatchingAttributes');
						game.save();
						res.send(201 , response);

						return next();		
				});
		}


		
	  } else {
		   res.send(404)
		   return next();
	  }
	 });
	   
}

function generateQuestion(game, response, callback){
	Animal.aggregate( [
	    { $unwind : "$attributes" },
	    { $group : { _id : "$attributes" , number : { $sum : 1 } } },
	    { $sort : { number : -1 } },
	    { $match: {number : { $gt: 3 } } }
		  ], function (err, attributes) {
	        if(err){
	            return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)));
	        }

	        if(attributes) {
	        	console.log('Guessed attributes: ' + game.guessedAttributes);
	        	console.log('Attributes length: ' + attributes.length);	
	        	
	        	//ask a random question every 3 to narrow things down faster
	        	if (game.currentQuestion % 3 == 0) {
				
		        	while (true) {
			        	var attributeIndex = Math.floor((Math.random() * attributes.length));
						var attribute = attributes[attributeIndex];	
						
						var index = game.guessedAttributes.indexOf(attribute._id);
						
						if (index === -1) {
							console.log('Attribute to guess: ' + attribute._id);
							game.guessedAttributes.push(attribute._id);
							game.currentAttribute = attribute._id;
							response = new GameResponse(game.currentQuestion, "Question", getRandomContextQuestion() + game.currentAttribute + '?');
							callback(response);
							return;
		            	}	
		        	}
	        	} else {

		            attributes.every(function(value) {
		            		console.log('Current attribute: ' + value._id + ' type: ' + typeof(value._id));
		            		var index = game.guessedAttributes.indexOf(value._id);
		            		if (index === -1) {
								console.log('Attribute to guess: ' + value._id);
								game.guessedAttributes.push(value._id);
								game.currentAttribute = value._id;
								response = new GameResponse(game.currentQuestion, "Question", getRandomContextQuestion() + game.currentAttribute + '?');
								callback(response);
								return false;
		            		}

		            		return true;
		            });
	        	}

	        } else {
	            return next(new restify.InvalidArgumentError(JSON.stringify('Could not find attributes.')))
	        }
	    } 
	);
}

function generateAnswer(game, response, callback) {

	if (true === game.bruteForce) {
		game.answer = game.bruteAnimalsToGuess.shift();
		if (typeof(game.answer) === 'undefined') {
			game.confirmedAttributes = []; 
			game.bruteForce = false;
			generateQuestion(game, response, callback);
			return;
		}

		var response = new GameResponse(game.currentQuestion, "Question", 'Are you a(n) ' + game.answer + '?');
		callback(response);
		return;
	}

	
	Animal.find( {attributes: {$all: game.confirmedAttributes }}, function (error, animals) {
	  if (error) {
	  	return next(new restify.InvalidArgumentError(JSON.stringify(error.errors)))
	  }

	  	animalsLeft = buildBruteForceAnimalList(animals);
	  	if (animalsLeft && animalsLeft.length > 0 && animalsLeft.length < 4 && ((20 - game.currentQuestion > animalsLeft.length) || game.currentQuestion === 20)) {
			game.bruteForce = true;
			game.answer = animalsLeft.shift();
			game.bruteAnimalsToGuess = animalsLeft;
			console.log('Guessing: ' + game.answer);
			var response = new GameResponse(game.currentQuestion, "Question", 'Are you a(n) ' + game.answer + '?');
			callback(response);
		} else if (animalsLeft && animalsLeft.length === 0 ){
			game.confirmedAttributes = [];
		}

	});
}

function generateAnswer2(game, response, callback) {

	var keys = Object.keys(game.animalsMatchingAttributes);
	console.log('Total matching animals: ' + keys.length);
	if (game.currentQuestion  < 10) {
		return;
	}

	if (keys.length > 0) {
		keys = keys.sort(function (a, b) {
			return game.animalsMatchingAttributes[b][b] - game.animalsMatchingAttributes[a][a];
		});

		keys.every(function(key) {
			console.log('Animal: ' + key + ' count: ' + game.animalsMatchingAttributes[key][key]);
			var value = game.animalsMatchingAttributes[key][key];
			if (value > 3 || game.currentQuestion >= 18) {
				console.log('First animal matching: ' + key);
				var index = game.guessedAnimals.indexOf(key);
	    		if (index === -1) {
	    			game.answer = key;
					console.log('Animal to guess: ' + value._id);
					var response = new GameResponse(game.currentQuestion, "Question", 'Are you a(n) ' + game.answer + '?');
					callback(response);
					return false;
	    		}
			} 

			return true;
		});
		/*
		game.animalsMatchingAttributes.sort( function(a, b) {
			return 
		}
		).reverse();

		//check guessed animals
		if (game.animalsMatchingAttributes[0] > 2) {
			console.log('First animal matching: ' + game.animalsMatchingAttributes[0]);
			var index = game.guessedAnimals.indexOf(game.animalsMatchingAttributes[0]);
    		if (index === -1) {
    			game.answer = game.animalsMatchingAttributes[0];
    			game.guessedAnimals.push(game.answer);
				console.log('Animal to guess: ' + value._id);
				var response = new GameResponse(game.currentQuestion, "Question", 'Are you a(n) ' + game.answer + '?');
				callback(response);
				return false;
    		}
		} 
		*/
	} else {
		callback(null);
		return;
	}
}

function buildBruteForceAnimalList(animals){
	var bruteAnimalsToGuess = [];

	animals.forEach(function(value) {
	  	console.log(value.name);
		bruteAnimalsToGuess.push(value.name);
	});

	return bruteAnimalsToGuess;
}

function getRandomContextQuestion() {
	var questions = [
		'Is this word one of your attributes: ',
		'Does this word describe you: '
	];

	var questionIndex = Math.floor((Math.random() * questions.length));

	return questions[questionIndex];

}
