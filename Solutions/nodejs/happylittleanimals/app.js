var application_root = __dirname,
    path = require("path"),
    mongoose = require('mongoose');

var express = require('express'), 
	http = require('http');

var app = express(); 
var server = http.createServer(app);

// Database

mongoose.connect('mongodb://localhost:27017/animals');


var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
  // yay!
});

var animalSchema = mongoose.Schema({
    name: String,
    attributes: [String]
});

var Animal = mongoose.model('Animal', animalSchema);

console.log('Removing data...');
Animal.remove({}, function(err) {
	if (err) {
		console.log(err);
	}
});

function readLines(input, func) {
  var remaining = '';

  input.on('data', function(data) {
    remaining += data;
    var index = remaining.indexOf('\n');
    var last  = 0;
    while (index > -1) {
      var line = remaining.substring(last, index);
      last = index + 1;
      func(line);
      index = remaining.indexOf('\n', last);
    }

    remaining = remaining.substring(last);
  });

  input.on('end', function() {
    if (remaining.length > 0) {
      func(remaining);
    }
  });
}

function storeAttributes(data) {
	var name = data.replace(/\r$/,'').toLowerCase();

	var http = require("http");
    url = "http://api.wordnik.com:80/v4/word.json/" + name + "/relatedWords?useCanonical=false&relationshipTypes=same-context&limitPerRelationshipType=100&api_key=<need api key>";

    console.log(url);

	// get is a simple wrapper for request()
	// which sets the http method to GET
	var request = http.get(url, function (response) {
	    // data is streamed in chunks from the server
	    // so we have to handle the "data" event    
	    var buffer = "", 
	        data,
	        route;

	    response.on("data", function (chunk) {
	        buffer += chunk;
	    }); 

	    response.on("end", function (err) {
	        // finished transferring data
	        // dump the raw data
	        console.log(name + ': ' + buffer + "\n Parsed: " + JSON.parse(buffer));
	        console.log("\n");
	        data = JSON.parse(buffer);
	        var attributes;

	        if (typeof(data) !== 'undefined' && typeof(data[0]) !== 'undefined') {
	        	attributes = data[0].words;	
	        }

	        var animal = new Animal({ name: name, attributes: attributes  });
			animal.save(function (err, animal) {
			  if (err) return console.error(err);
			  console.log('Saved: ' + animal);
			});
	    }); 
	}); 
};

var fs = require('fs');
var input = fs.createReadStream('<path to animals.txt>');
readLines(input, storeAttributes);


