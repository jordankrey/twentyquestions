void GenerateRandomAnimals() 
{
	var text = System.IO.File.ReadAllText("C:\\buildium\\src\\twentyquestions\\DataFiles\\animals.txt");
	var list = text.Split(new[] {'\n'}, System.StringSplitOptions.RemoveEmptyEntries);
	var random = new Random(Guid.NewGuid().GetHashCode());

	while (true) 
	{
		var index = random.Next(0, list.Count() - 1);

		Console.WriteLine("Hit enter to get a randomly selected animal. Type exit to stop.");
		var input = Console.ReadLine();
		if (input == "exit") return;
		Console.WriteLine("Your randomly selected animal is: " + list[index] + "\n");
	}
}
GenerateRandomAnimals();