﻿namespace TwentyQuestionsClient.Models
{
    public class GameNameResponse
    {
        public GameNameResponse(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}