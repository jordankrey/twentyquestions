﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace TwentyQuestionsClient.Models
{
    public class GameResponse
    {
        public enum ResponseType
        {
            Question,
            GiveUp,
        };

        public enum Answer
        {
            Yes,
            No,
            Skip,
            YesWin,
            NoEnd,        
        };

        [JsonConverter(typeof(StringEnumConverter))]
        public ResponseType Type { get; set; }
        public int Id { get; set; }
        public string Question { get; set; }
    }
}