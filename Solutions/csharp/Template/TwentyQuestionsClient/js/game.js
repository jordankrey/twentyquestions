/// <reference path="jquery.d.ts" />

var GameRunner = (function () {
    function GameRunner(baseUrl) {
        this.baseUrl = baseUrl;
        this.uuid = uuid;
    }
    GameRunner.prototype.initialize = function () {
        var obj = this;
        var result;
        this.count = 0;
        result = $.ajax({
            url: this.baseUrl + "/name",
            crossDomain: true,
            dataType: 'json'
        }).then(function (data) {
            obj.name = data.Name;
        });
        return result;
    };
    GameRunner.prototype.startGame = function () {
        this.gameId = this.uuid.v1();

        return this.callService(this.baseUrl + "/game/" + this.gameId + "/start");
    };
    GameRunner.prototype.sendYes = function () {
        return this.callService(this.baseUrl + "/game/" + this.gameId + "/answer/" + this.questionId + "/yes");
    };
    GameRunner.prototype.sendNo = function () {
        return this.callService(this.baseUrl + "/game/" + this.gameId + "/answer/" + this.questionId + "/no");
    };
    GameRunner.prototype.sendSkip = function () {
        this.count = this.count - 1;
        return this.callService(this.baseUrl + "/game/" + this.gameId + "/answer/" + this.questionId + "/skip");
    };
    GameRunner.prototype.sendWin = function () {
        return $.ajax({
            url: this.baseUrl + "/game/" + this.gameId + "/answer/" + this.questionId + "/win",
            type: "POST",
            crossDomain: true,
            dataType: 'json'
        });
    };
    GameRunner.prototype.sendLose = function () {
        return $.ajax({
            url: this.baseUrl + "/game/" + this.gameId + "/answer/" + this.questionId + "/lose",
            type: "POST",
            crossDomain: true,
            dataType: 'json'
        });
    };
    GameRunner.prototype.handleQuestion = function (response) {
        this.questionId = response.Id;
        return response;
    };
    GameRunner.prototype.callService = function (url) {
        var obj = this;
        var result;

        if (this.count > 20) {
            this.sendLose();
            return { Type: "GameOver" };
        }

        result = $.ajax({
            url: url,
            type: "POST",
            crossDomain: true,
            dataType: 'json'
        }).then(function (data) {
            obj.questionId = data.Id;
            obj.count = obj.count + 1;
            return data;
        });
        return result;
    };
    return GameRunner;
})();
//# sourceMappingURL=game.js.map
