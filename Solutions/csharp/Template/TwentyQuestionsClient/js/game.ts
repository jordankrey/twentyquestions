/// <reference path="jquery.d.ts" />
interface IUuid { v1() : string; }
declare var uuid : IUuid;
class GameRunner {
	
	name: string;
	gameId: string;
	uuid : IUuid;
	questionId: number;
	count: number;

	constructor(public baseUrl) {
		this.uuid = uuid;
	}	
	initialize() {
		var obj = this;
		var result;
		this.count = 0;
		result = $.ajax({
			url: this.baseUrl + "/name", 
			crossDomain: true,
			dataType: 'json'
		}).then(function(data) { obj.name = data.Name; });
		return result;
	}
	startGame() {
		this.gameId = this.uuid.v1();

		return this.callService(this.baseUrl + "/game/" + this.gameId + "/start");		
	}
	sendYes() {
		return this.callService(this.baseUrl + "/game/" + this.gameId + "/answer/" + this.questionId + "/yes");
	}
	sendNo() {
		return this.callService(this.baseUrl + "/game/" + this.gameId + "/answer/" + this.questionId + "/no");
	}
	sendSkip() {
		this.count = this.count - 1;
		return this.callService(this.baseUrl + "/game/" + this.gameId + "/answer/" + this.questionId + "/skip");	
	}
	sendWin() {
		return $.ajax({
			url: this.baseUrl + "/game/" + this.gameId + "/answer/" + this.questionId + "/win",
			type: "POST",
			crossDomain: true,
			dataType: 'json'
		});		
	}
	sendLose() {
		return $.ajax({
			url: this.baseUrl + "/game/" + this.gameId + "/answer/" + this.questionId + "/lose",
			type: "POST",
			crossDomain: true,
			dataType: 'json'
		});		
	}
	handleQuestion(response) {		
        this.questionId = response.Id;
        return response;        
	}
	callService(url: string) {
		var obj = this;
		var result;

		if (this.count > 20) {
			this.sendLose();
			return { Type: "GameOver" };
		}

		result = 
			$.ajax({
				url: url,
				type: "POST",
				crossDomain: true,
				dataType: 'json'
			}).then(function(data) { 
				obj.questionId = data.Id;
				obj.count = obj.count + 1;
				return data;
			});
		return result;
	}
}


